import { UsersModule } from './../users/users.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '../../../node_modules/@angular/forms';


@NgModule({
  imports: [
UsersModule,
CommonModule, AngularFontAwesomeModule,FormsModule
  ],
  declarations: [LoginComponent],
  exports:[
    LoginComponent
  ]
})
export class AuthModule { }
