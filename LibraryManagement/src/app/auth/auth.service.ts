import { AddserviceService } from './../addservice.service';
import { CookieService } from 'ngx-cookie-service';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
users;
email:string;
currentmail:any;
  constructor(private router:Router, private service:AddserviceService, private cookieService: CookieService) { 
    this.getdata()
  }

  cookieValue = 'UNKNOWN';
  getdata() {
    this.service.getCoins().subscribe(res => {
      this.users = res;
    });
  }

  logg:boolean

  public loginchangeEvent : Subject<string> = new Subject<string>();
  public adminchangeEvent : Subject<boolean> = new Subject<boolean>();
  role="";
    
   login(user):boolean{
    
    // console.log(this.users);
    this.email=user.mail;
     const res = this.users.find((u)=>{
       return u.mail === user.mail && u.pass === user.pass;
     });
    
     if(res){
      
       localStorage.setItem('userEmail',user.mail);
      this.logg=true;
      this.currentmail=user.mail;
      
      
    //  this.loginchangeEvent.next('isloggedIn');

       if(user.mail === "admin"){
        sessionStorage.setItem('userEmail',user.mail+"-admin");
        // console.log(user.mail)
         this.adminchangeEvent.next(true);
        // console.log(this.adminchangeEvent)
       }
       else{
        sessionStorage.setItem('userEmail',user.mail+"-user");
        this.adminchangeEvent.next(false);
       }
       this.router.navigate(['/dashboard']);
     return true;
        
     }
     console.log("Invalid User details")
     this.logg=false;
     return false;
   }
    isLoggedIn(){
     
    const userEmail = sessionStorage.getItem('userEmail')
    return userEmail ? true:false;
    }
    logout(){
    sessionStorage.clear();
     localStorage.clear();
      
    }
    getCurrentUser(){
      // if(!(sessionStorage.getItem('userEmail')==null) ){
        
      
    
      return sessionStorage.getItem('userEmail').split('-')[1] == "admin" ? true : false;}
    // }
}


