import { HttpClient } from '@angular/common/http';
import { AuthService } from './../../auth.service';
import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '../../../../../node_modules/@angular/forms';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  constructor(private router:Router, private service:AddserviceService,private autservice:AuthService) { }
  
  ngOnInit() {
    // window.history.go(-1);
  }
  
  onLoginSubmit(myForm:NgForm){
    if(myForm.valid){
    this.autservice.login(myForm.value);
   }
    else{
     console.log(myForm.value);

    }
 }

  // getdata() {
  //   this.service.getCoins().subscribe(res => {
  //     this.users = res;
  //   });
  // }

  

  // // public loginchangeEvent : Subject<string> = new Subject<string>();


  //  login(a,b):boolean{
    

  //    const res = this.users.find((u)=>{
  //      return u.mail === a && u.pass === b;
  //    });
     

  //    if(res){
  //    sessionStorage.setItem("userEmail",this.users.mail);
  //    this.router.navigate(['/dashboard']);
    
  //    return true;
        
  //    }
  //    console.log("Invalid User details")
  //    return false;
  //  }
  //   isLoggedIn(){
     
  //   const userEmail = sessionStorage.getItem('userEmail')
  //   return userEmail ? true:false;
  //   }
  //   logout(){
  //   sessionStorage.clear();
      
  //   }

}
