import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBooksComponent } from './components/add-books/add-books.component';
import { ViewBooksComponent } from './components/view-books/view-books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesComponent } from './components/categories/categories.component';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        Ng2SearchPipeModule
  ],
  exports:[
    AddBooksComponent,ViewBooksComponent,CategoriesComponent
  ],
  declarations: [AddBooksComponent, ViewBooksComponent, CategoriesComponent]
})
export class BooksModule { }
