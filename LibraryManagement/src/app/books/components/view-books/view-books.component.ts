import { MessageComponent } from './../../../users/components/message/message.component';
import { MessageService } from './../../../message.service';
import { AuthService } from './../../../auth/auth.service';
import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-view-books',
  templateUrl: './view-books.component.html',
  styleUrls: ['./view-books.component.css']
})
export class ViewBooksComponent implements OnInit {
  books;
  users;
  currentUser;
  val;
  admin=true;
  buttonDisabled:boolean;
  disable;
  disableButton:any;
  cats:any;
  book:any;
  constructor(private service:AddserviceService, private auth:AuthService,private msg:MessageService) {
    this.buttonDisabled = false;
  }

  ngOnInit() {

    
    this.getdata();
    this.getuser();
    this.viewcate();
    this.getbook();
    this.admin = this.auth.getCurrentUser();
    this.auth.adminchangeEvent.subscribe(admin =>{
    this.admin = admin;
    // console.log("hii")
    
    });
    
  }
  getbook()
  {
    this.service.getBooks().subscribe(res => {
      this.book = res;
      });
  }
  viewcate()
  {
    this.service.getCate().subscribe(res => {
      this.cats = res;
      });
  }
   
  
  getdata() {
    this.service.getBooks().subscribe(res => {
    this.books = res;
    console.log(this.books);
    });
   
    }
    getuser() {
      this.service.getCoins().subscribe(res => {
      this.users = res;
      });
      }
// user=this.msg.getcurData()[1].mail;
// bookId=this.msg.getcurData()[0].bookId;
// bookName=this.msg.getcurData()[0].bookName;
// category=this.msg.getcurData()[0].category;
  status="Pending"
      details(ip,i){
        // this.msg.details(ip);
        // console.log(this.user)
      console.log(ip.bookId +"--"+ sessionStorage.getItem('userEmail').split("-")[0]);
      this.currentUser=sessionStorage.getItem('userEmail').split("-")[0];

        const res = this.users.find((u)=>{
          return u.mail === this.currentUser;
        });
       console.log(res.pass) 

     
       this.addMsgHere(ip.bookId,ip.bookName, ip.categoryName, res.mail, this.status);
        
      //  this.disable=true;
      // i.disableButton=true;
       
      }
         
        addMsgHere(bookId, bookName, category, user, status) {
        this.service.addMsg(bookId, bookName, category, user, status);
        
      }
      filterForeCasts(filterVal: any) {
        if(this.books.categoryName != filterVal)
        {
          this.book=null;
        }
        if (filterVal == "All")
        {
          this.book=this.books;
        }
         
        else
        {
          this.book = this.books.filter((item) => item.categoryName.indexOf(filterVal) !== -1);
        }    
    }
    q: number = 1;
    

}
