import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})
export class CategoriesComponent implements OnInit {
title="ADD CATEGORY";
catForm: FormGroup;
  constructor(private service:AddserviceService,private fb: FormBuilder) {this.createForm() }
  createForm() {
    this.catForm = this.fb.group({
      cat: ['', Validators.required ],
   });
  }
  ngOnInit() {
    
  }
  addCat(categ) {
    this.service.addCate(categ);
    console.log(categ);
}

}

