import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-books',
  templateUrl: './add-books.component.html',
  styleUrls: ['./add-books.component.css']
})
export class AddBooksComponent implements OnInit {
  
  title = 'Add Books';
   angForm: FormGroup;
   cates:any;
   nbooks;
  constructor(private addservice: AddserviceService, private fb: FormBuilder, private router: Router) {
    this.getdata();
    
    this.createForm();
       }
         createForm() {
    this.angForm = this.fb.group({
      bookId: ['', Validators.required ],
      category: ['', Validators.required ],
      author: ['', Validators.required ],
      publ: ['', Validators.required ],
      count: ['', Validators.required ],

      bookName: ['', Validators.required ]
   });
  }
  val;
  addBook(bookId, bookName, category, author, publ, count) {
      // this.checkId(bookId, bookName, category, author, publ, count);
      const res = this.nbooks.find((u)=>{
        return u.bookId == bookId ;
        // console.log(u.bookId + "now "+ bookId)
      });
       
         if(res){
          console.log("Already exists")
           alert("Book Id already exists")
         }
        else
         {
          console.log("hi "+bookId)
          this.addservice.addBook(bookId, bookName, category, author, publ, count);
         }
  }
    // checkId(bookId, bookName, category, author, publ, count):boolean{
    //  this.res = this.nbooks.find((u)=>{
    //   return u.bookId === bookId ;
    // });
     
    //    if(this.res){
    //     console.log("Already exists")
    //      alert("Book Id already exists")
    //      return true;
    //    }
      
    //     console.log("hi "+bookId)
    //   this.addservice.addBook(bookId, bookName, category, author, publ, count);
    //   return false;
    // }

    checkId(bookId, bookName, category, author, publ, count){

     
     }
  getcate(){
    this.addservice.getCate().subscribe(res => {
      this.cates = res;
    });
  }
  getdata() {
    this.addservice.getBooks().subscribe(res => {
    this.nbooks = res;
    // console.log(this.nbooks);
    });
   
    }

  ngOnInit() {
    this.getcate();
  }

}
