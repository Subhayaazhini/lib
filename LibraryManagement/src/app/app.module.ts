import { MessageService } from './message.service';
import { BooksModule } from './books/books.module';
import { ApproutingModule } from './approuting.module';
import { UsersModule } from './users/users.module';
import { AddserviceService } from './addservice.service';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

// import { TitleCasePipe } from '@angular/common';




// import {AddUsersComponent} from './users/components/add-users/add-users.component'

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AuthModule,
    AngularFontAwesomeModule,
    HttpClientModule,
   ReactiveFormsModule,
   UsersModule,
   RouterModule,
   ApproutingModule,
   BooksModule,
  //  TitleCasePipe
    
  ],
  // exports: [
  //   AppComponent
  // ],
  providers: [AddserviceService,MessageService,CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }


