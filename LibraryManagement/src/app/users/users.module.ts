import { ApproutingModule } from './../approuting.module';
import { BooksModule } from './../books/books.module';
// import { AuthModule } from './../auth/auth.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddUsersComponent } from './components/add-users/add-users.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewUsersComponent } from './components/view-users/view-users.component';
import { EditUsersComponent } from './components/edit-users/edit-users.component';
import { MessageComponent } from './components/message/message.component';
import { UserComponent } from './components/user/user.component';
// import { CategoriesComponent } from '../books/components/categories/categories.component';
// import { AdminComponent } from './components/admin/admin.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
        ReactiveFormsModule,
        BooksModule,
        ApproutingModule,
        Ng2SearchPipeModule
        // AuthModule

  ],
  exports:[
    AddUsersComponent,
  ],
  declarations: [DashboardComponent, AddUsersComponent, ViewUsersComponent, EditUsersComponent, MessageComponent, UserComponent]
})
export class UsersModule { }
