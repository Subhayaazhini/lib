import { Subject } from 'rxjs';
import { AddserviceService } from './../../../addservice.service';
import { AuthService } from './../../../auth/auth.service';
import { MessageService } from './../../../message.service';
import { ViewBooksComponent } from './../../../books/components/view-books/view-books.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';





@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  public approvechangeEvent : Subject<boolean> = new Subject<boolean>();
  approv=false;
request:any
  constructor(private msg:MessageService ,private auth:AuthService, private addservice:AddserviceService,private route: ActivatedRoute, private router: Router) {  this.buttonDisabled = false;}
arr;
admin;
message;
msgg:any;
books
// disableButton;
buttonDisabled:boolean;
// email:any;
  ngOnInit() {
  // this.arr=this.msg.getcurData()[0].concat(this.msg.getcurData()[1])
    // console.log(this.msg.getcurData()[0] );
    // console.log(this.sessBook)

    
    // this.route.params.subscribe(params => {
    //   this.msgg = this.addservice.editMsg(params['id']).subscribe(res => {
    //     this.msgg = res;
    //   });
    // });
   this.getmsg();
   this.getbook();
   

    this.approv = this.changeApprove();
    this.approvechangeEvent.subscribe(approv =>{
    this.approv = approv;
  });

   this.admin = this.auth.getCurrentUser();
    this.auth.adminchangeEvent.subscribe(admin =>{
    this.admin = admin;
  });

  
  }
  
  getmsg() {
    this.addservice.getMsg().subscribe(res => {
    this.message = res;
    });
    }

    getbook() {
      this.addservice.getBooks().subscribe(res => {
      this.books = res;
      });
      }
// user=this.msg.getcurData()[1].mail;
// bookId=this.msg.getcurData()[0].bookId;
// bookName=this.msg.getcurData()[0].bookName;
// category=this.msg.getcurData()[0].category;
sessBook=localStorage.getItem("sessBook");
  // approve(ip){
     
  //   console.log(ip.bookId);
  
  //   localStorage.setItem("sessBook",ip.bookId);
  //   sessionStorage.setItem("sessBookId",ip.bookId);
  //   this.approvechangeEvent.next(true);
  //   console.log(this.changeApprove())
  //   console.log(this.sessBook)
  // }
 newStatus="Approved"
  approve(ip){
    this.updateMsg(ip.bookId, ip.bookName, ip.category, ip.user, this.newStatus,ip._id);
    // this.disableButton = true;
    // console.log(ip.bookId);
    // console.log(ip._id);
     
    const res = this.books.find((u)=>{
      return u.bookId === ip.bookId;
    });

     console.log(res.count-1+"  "+res._id)
     this.updateBook(res.bookId, res.bookName, res.categoryName, res.author, res.publ, res.count-1,res._id);
    
  }

  updateMsg(bookId, bookName, category, user, status,id) {

    this.route.params.subscribe(params => {
    this.addservice.updateMsg(bookId, bookName, category, user, status, id);
    
  });
}
   
updateBook(bookId, bookName, category, author, publ,count,id) {

  this.route.params.subscribe(params => {
  this.addservice.updateBook(bookId, bookName, category, author, publ,count, id);
  
});
}

       
  changeApprove(){
    const sess=sessionStorage.getItem("sessBookId")
     return (sess? true:false);
  }

}
