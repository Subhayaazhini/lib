var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items


var Msg = new Schema({
  bookId: {
    type: Number
  },
  bookName: {
    type: String
  },
  category: {
    type: String
  },
  user: {
    type: String
  },
  status: {
    type: String
  },
  
  
},{
    collection: 'Msg'
});
 module.exports = mongoose.model('Msg', Msg);
